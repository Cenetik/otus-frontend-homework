import axios, { AxiosResponse, AxiosRequestConfig, RawAxiosRequestHeaders } from 'axios';
import { Component } from "react";
import './LoginForm.scss'

interface LoginFormState {    
    login: string;
    password: string;
}

const client = axios.create({
  baseURL: 'https://localhost:7126/api/v1/',
});

async function loginAsync(login:string,password:string)  {
 
  const config: AxiosRequestConfig = {
    headers: {
      'Accept': 'application/json',      
    } as RawAxiosRequestHeaders,
  };
  
  try {
    const data = {'login': login, 'password': password};
    console.log(data);   
    const response: AxiosResponse = await client.post(`/Auth`, data , config);
    console.log(response.status);
    console.log(response.data);   
    alert('sended to backend') 
  } catch(err) {
    console.log(err);
    alert('error login on backend' + err);
  } 
  
};

function login(login:string, password:string) {
  const config: AxiosRequestConfig = {
    headers: {
      'Accept': 'application/json',      
    } as RawAxiosRequestHeaders,
  };
  const data = {'login': login, 'password':password}
  console.log(data);   
  client.post('/Auth',data,config)
        .then((response)=>{
          console.log(response.status);
          console.log(response.data);
        })
        .catch((error)=>{
          console.log(error);
          alert('error login on backend' + error);
        });
};


export class LoginForm extends Component<{},LoginFormState> {
    constructor(props: any) {
        super(props);
        this.state = {
            login: "",
            password: ""            
        };
    }

    setLogin = (e: any) => {
        this.setState({ login: e.target.value });
    }

    setPassword = (e: any) => {
        this.setState({ password: e.target.value });
    }       

    render() {
      return <div className="container">
                <div className="form">
                  <div className="login-form">
                    <input type="text" name="username" placeholder="Username" onChange={this.setLogin}/>
                    <input type="text" name="password" placeholder="Password" onChange={this.setPassword}/>
                    <button className="btn" onClick={() => loginAsync(this.state.login,this.state.password)}>Login</button>             
                  </div>
                </div>
              </div>        
    }
}